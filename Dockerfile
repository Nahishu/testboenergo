FROM python:3.10.2-slim-buster
RUN mkdir -p /home/nahishu/docker/qr
WORKDIR /home/nahishu/docker/qr
COPY . /home/nahishu/docker/qr
RUN apt-get update && apt-get -y install libpq-dev gcc 
WORKDIR /home/nahishu/docker/qr
RUN pip3 install -r requirements.txt
EXPOSE 8000
CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]d

