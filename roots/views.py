from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from .utils import find_roots


def index(request):
    try:
        #Проверка пустого ввода
        if not request.GET:
            result = {
                'Answer': 'Необходимо ввести значения параметров '
                          'a,b,c в формате .../roots/?a=число&b=число&c=число'
            }
            return JsonResponse(result)

        else:
            a = float(request.GET.get('a', 0))
            b = float(request.GET.get('b', 0))
            c = float(request.GET.get('c', 0))
            result = find_roots(a=a, b=b, c=c)
            return JsonResponse(result)
    #Проверка допустимых значений параметров уравнения
    except ValueError:
        result = {
            'Answer': 'Значения параметров уравнения должны быть числами'
            }
        return JsonResponse(result)
