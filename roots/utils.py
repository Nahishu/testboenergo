def find_roots(a=0, b=0, c=0):
    #Линейное уравнение
    if a == 0 and b != 0:
        x = -c / b
        if x == 0:
            return {
            'Answer': 'Уравнение имеет единственный корень',
            'x': 0
        }
        response = {
            'Answer': 'Уравнение имеет единственный корень',
            'x': x
        }
        return response
    #Уравнение не имеет решения
    elif a == 0 and b == 0:
        response = {
            'Answer': 'Уравнение не имеет решений'
        }
        return response
    else:
        discriminant = b ** 2 - 4 * a * c
        #Проверка дискриминанта
        if discriminant < 0:
            response = {
                'Answer': 'Уравнение не имеет действительных решений'
            }
            return response
        #Дискриминант равен нулю
        elif discriminant == 0:
            x = (-b / (2 * a))
            response = {
                'Answer': 'Уравнение имеет единственный корень',
                'x': x
            }
            return response
            # Дискриминант больше нуля - решаем уравнение
        elif discriminant > 0:
            x1 = (-b + (b ** 2 - 4 * a * c) ** 0.5) / (2 * a)
            x2 = (-b - (b ** 2 - 4 * a * c) ** 0.5) / (2 * a)
            response = {
                'Answer': 'Уравнение имеет два корня',
                'x1': x1,
                'x2': x2
            }
            return response

