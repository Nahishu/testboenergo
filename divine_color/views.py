from django.http import JsonResponse
from .models import Objects_color
from .utils import chek_param

def index(request):
    try:
        # Валидация входящих параметров
        if chek_param(request.GET) != 'ok':
            return JsonResponse({
                'Error': chek_param(request.GET)
            })

        # Проверяем, что это первый запрос по данному номеру предмета
        if not request.GET.get('color'):
            # Проверка на наличие предмета с заданным номером в БД
            if Objects_color.objects.filter(user=request.GET.get('user', '0'),
                    obj_number=request.GET.get('number', 0)).exists():
                result = {'color': Objects_color.objects.get
                            (user=request.GET.get('user'),
                            obj_number=request.GET.get('number')).obj_color}
                # Возвращаем ответ с цветом предмета
                return JsonResponse(result)

            else:
        # Количество синих, зеленых, красных в БД на момент очередного запроса
                blue_in_db = Objects_color.objects.filter\
                (user=request.GET.get('user', '0'), obj_color='blue').count()

                green_in_db = Objects_color.objects.filter\
                (user=request.GET.get('user', '0'), obj_color='green').count()

                red_in_db = Objects_color.objects.filter\
                (user=request.GET.get('user', '0'), obj_color='red').count()

                # Задаем начальные значения синих, зеленых, красных в БД для
                # формирования порядка следования вопросов
                # (исходя из условия синие > зеленые > красные)
                if blue_in_db == 0 and green_in_db == 0 and red_in_db == 0:
                    blue_in_db, green_in_db, red_in_db = 3, 2, 1

                # Предполагаемое изначальное количество
                # синих, зеленых, красных
                blue_init = int((blue_in_db / (blue_in_db +
                                            green_in_db + red_in_db)) * 100)
                green_init = int((green_in_db / (blue_in_db +
                                            green_in_db + red_in_db)) * 100)
                red_init = int((red_in_db / (blue_in_db +
                                            green_in_db + red_in_db)) * 100)

                # Предполагаемое количество синих, зеленых, красных
                # на момент очередного запроса
                n_blue = blue_init - blue_in_db
                n_green = green_init - green_in_db
                n_red = red_init - red_in_db

                # Формирование очередности задаваемых вопросов
                question_item = {'blue': n_blue, 'green': n_green,
                                 'red': n_red}
                global order_of_question
                order_of_question = sorted(question_item.items(),
                                        key=lambda item: -item[1])

                answer = order_of_question[0][0]
                # Счетчик ответов по конкретному номеру предмета
                global question_counter
                question_counter = 1
                return JsonResponse({'color': answer})

        # Проверка номера вопроса по атрибу color
        elif request.GET.get('color'):
            if request.GET.get('answer') == 'true':
                question_counter = 0

                # Исключение дублей БД
                if Objects_color.objects.filter(user=request.GET.get('user'),
                    obj_number=request.GET.get('number'),
                            obj_color=request.GET.get('color')).exists():
                    return JsonResponse({'Error':
                                'Неправильная последовательность вопросов'})

                # Заносим правильный вариант в БД
                Objects_color.objects.create(user=request.GET.get('user'),
                    obj_number=request.GET.get('number'),
                                      obj_color=request.GET.get('color'))
                return JsonResponse({'Success': 'Введите следующий вопрос'})
            else:
                # Блоки ответов 2 и 3
                if question_counter == 1:
                    answer = order_of_question[1][0]
                    question_counter = 2
                    return JsonResponse({'color': answer})
                elif question_counter == 2:
                    answer = order_of_question[2][0]
                    question_counter = 0
                    Objects_color.objects.create(user=request.GET.get('user'),
                                        obj_number=request.GET.get('number'),
                                        obj_color=order_of_question[2][0])
                    return JsonResponse({'color': answer,
                                         'success': 'Введите следующий вопрос'})
    except NameError:
        return JsonResponse({'Error': 'Неправильная последовательность вопросов'})












