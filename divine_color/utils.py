def chek_param(param_dict):
    color_list = ['green', 'blue', 'red']
    answer_list = ['true', 'false']
    question_number_list = [x for x in range(101)]

    if 'user' and 'number' not in param_dict:
        return 'Необходимо ввести имя пользователя : user=username, ' \
               'и номер предмета: number=...'

    if int(param_dict.get('number')) not in question_number_list:
        return 'Номера предметов должны быть положительными целыми числами ' \
               'от 1 до 100'

    if param_dict.get('color'):
        if param_dict.get('color') not in color_list:
            return 'Значения цветов могут быть только : blue, green, red'

    if param_dict.get('answer'):
        if param_dict.get('answer') not in answer_list:
            return 'Ответ должен быть true или false'

    return 'ok'
