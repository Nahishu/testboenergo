from django.apps import AppConfig


class DivineColorConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'divine_color'
