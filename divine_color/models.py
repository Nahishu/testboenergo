from django.db import models

class Objects_color(models.Model):
    user = models.CharField(max_length=50)
    obj_number = models.IntegerField()
    obj_color = models.CharField(max_length=50)
